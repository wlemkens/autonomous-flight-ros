#!/usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time

	
def plan_path():
	global moveit_commander
	rate = rospy.Rate(1) # 1hz
	robot = moveit_commander.RobotCommander()
	scene = moveit_commander.PlanningSceneInterface()
	group = moveit_commander.MoveGroupCommander("Quadrotore")
	display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory, queue_size=1)
	print "============ Reference frame: %s" % group.get_planning_frame()
	print "============ Robot Groups:"
	print robot.get_group_names()
	print "============ Printing robot state"
	print robot.get_current_state()
	print "============ Generating plan 1"
	
	group.set_planner_id("RRTConnectkConfigDefault");

	group.allow_replanning(True)
	
	print "============ Setting up workspace. Make sure the bounds are valid for your simulation, or it won't work."
	ybounds = 5
	height = 6
	returnheight = 4
	group.set_workspace ([-1, -ybounds, 0, 16, ybounds, height])
	group_variable_values = group.get_current_joint_values()
	print "============ Joint values: ", group_variable_values
	group_variable_values[0] = 14.0
	group_variable_values[2] = 0.5
	group.set_joint_value_target(group_variable_values)

	plan1 = group.plan()
	result = group.go(group_variable_values,wait=True)

if __name__ == '__main__':
	global move_publisher, moveit_commander
	print "============ Starting tutorial setup"
	moveit_commander.roscpp_initialize(sys.argv)
	rospy.init_node('plan_path', anonymous=True)
	move_publisher = rospy.Publisher('/cmd_vel',geometry_msgs.msg.Twist, queue_size=1)
	plan_path()
		
	
